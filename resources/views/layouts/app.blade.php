<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('includes.meta')

        <title>
            @if(View::hasSection('title'))
                @yield('title') &nbsp; | &nbsp; Coalition Technologies
            @else
                Coalition Technologies
            @endif
        </title>

        @include('includes.styles')
    </head>
    <body>
        <div id="app">
            @yield('content')
        </div>
        @include('includes.scripts')
        @yield('scripts')
    </body>
</html>
