@extends('layouts.app')

@section('title','Products')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-center">Products</h1>
            <hr>
            <h2>Create new Product</h2>
            <form id="create-product-form">
                <div class="form-group">
                    <label>Name</label>
                    <input type="string" class="form-control" name="name" placeholder="Enter name">
                </div>
                <div class="form-group">
                    <label>Quantity</label>
                    <input type="number" class="form-control" name="quantity" placeholder="Enter Quantity">
                </div>
                <div class="form-group">
                    <label>Price</label>
                    <input type="number" class="form-control" name="price" placeholder="Enter Price">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            <hr>
            <div>
                <table class="table table-striped table-inverse">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Total Value</th>
                            <th>Created At</th>
                        </tr>
                    </thead>
                    <tbody id="products-table"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        getProducts();
    });

    function getProducts() {
        $.ajax({
            type: 'GET',
            url: '/products',
            success: function (data) {
                $('#products-table').empty();

                let totalValueOfAllProducts = 0;

                for (let i = 0; i < data['data'].length; i++) {
                    const product = data['data'][i];

                    let totalValue = product.price * product.quantity;

                    totalValueOfAllProducts += totalValue;

                    let html = '<tr><th scope="row">' + product.name + '</th><td>' + product.price +
                            '</td><td>' + product.quantity + '</td><td>' + totalValue + '</td><td>' + product.created_at + '</td></tr>';
                    $('#products-table').append(html);
                }
                
                $('#products-table').append('<tr class="font-weight-bold"><td colspan="4">TOTAL VALUE OF ALL PRODUCTS:</td><td>' + totalValueOfAllProducts + '</td></tr>');
            },
            error: function () {
                console.log('Could not get the data');
            }
        });
    }


    $('#create-product-form > button').on('click', function (e) {
        e.preventDefault();

        data = $('#create-product-form').serializeArray();

        $.ajax({
            type: 'POST',
            url: '/products',
            data: data,
            success: function () {
                console.log('Successfully created');
                getProducts();
            },
            error: function () {
                console.log('Could not create');
            }
        });
    });
</script>
@endsection
