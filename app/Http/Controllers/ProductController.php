<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;
use App\Models\Product;

class ProductController extends BaseController {

    private $product;

    public function __construct(Product $product) {
        $this->product = $product;
    }

    public function index() {
        return view('index');
    }

    public function getProducts() {
        $data = $this->product->orderByDesc('created_at')->get()->values();

        return response()->json(['data' => $data]);
    }

    public function create(Request $request) {
        $product = new Product();
        $product->name = $request->name;
        $product->price = $request->price;
        $product->quantity = $request->quantity;
        $product->save();

        return response()->json(['data' => $product]);
    }

    public function edit(Request $request) {
        $product = $this->product->findOrFail($request->id);
        $product->name = $request->name;
        $product->price = $request->price;
        $product->quantity = $request->quantity;
        $product->save();

        return response()->json(['data' => $product]);
    }
}
